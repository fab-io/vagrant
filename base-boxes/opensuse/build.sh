#!/bin/bash -x

if [ -z ${ATLAS_TOKEN+x} ]
then 
	echo "ATLAS_TOKEN not set"
	exit 1
fi

tumbleweed_current_iso_url="http://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-NET-x86_64-Current.iso"

tumbleweed_snapshot_iso_url=$(curl --silent --head "$tumbleweed_current_iso_url" | egrep "^Location:" | awk '{print $2}')
tumbleweed_snapshot_checksum_url="$tumbleweed_snapshot_iso_url.sha256"

common_prefix=$(printf "%s\n%s\n" "$tumbleweed_current_iso_url" "$tumbleweed_snapshot_iso_url" | sed -e 'N;s/^\(.*\).*\n\1.*$/\1/')

snapshot_suffix=${tumbleweed_snapshot_iso_url#$common_prefix}

tumbleweed_version=${snapshot_suffix%-*}

basebox_version=${tumbleweed_version#Snapshot}

if [ -z "$basebox_version" ]
then
	echo "Unable to detect openSUSE version"
	exit 2
fi

packer build \
	-var "tumbleweed_iso_url=$tumbleweed_snapshot_iso_url" \
	-var "tumbleweed_checksum_url=$tumbleweed_snapshot_checksum_url" \
	-var "basebox_version=$basebox_version" \
	opensuse-basebox.json
