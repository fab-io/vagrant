#!/bin/bash

cd /home/vagrant

mkdir .ssh

mv /tmp/file_uploads/vagrant.pub .ssh/authorized_keys

chmod 700 .ssh

chmod 600 .ssh/authorized_keys

